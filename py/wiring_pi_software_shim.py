import os
import sys

import telnetlib

HOST="led_strip"


class WiringPiStub:
    @classmethod
    def import_wiringpi2(cls, logger):
        cls._is_stubbed = True
        logger.info("Using software GPIO/PWM control")
        return WiringPiStub(logger)

    @classmethod
    def is_stubbed(cls):
      return cls._is_stubbed

    def __init__(self, logger):
        self._logger = logger
        self.tn = None

    # TODO - Find a way to "method_missing" all of this...

    # Setup
    def wiringPiSetup(self, *args):
        if self.tn is None:
            print "wiringPiSetup - telnetlib"
            self.tn = telnetlib.Telnet(HOST)
            self.tn.write('NOOP\n')
            self.tn.read_until('ERROR')

    def wiringPiSetupSys(self, *args):
        print "wiringPiSetupSys - unknown"

    def pinMode(self, *args):
        print "pinMode - ignored"

    # Pin Writes
    def softPwmCreate(self, *args):
        print "softPwmCreate - ignored"

    def softPwmWrite(self, pin, pwm):
        #sys.stdout.write('softPwmWrite - set pin {:d} to {:d} ...'.format(pin, pwm))
        cmd = ''
        if pin == 0:
            cmd = 'RED {:d}\n'.format(pwm)
        elif pin == 1:
            cmd = 'GREEN {:d}\n'.format(pwm)
        elif pin == 2:
            cmd = 'BLUE {:d}\n'.format(pwm)
        else:
            raise ValueError('pin out of expected range')
        self.tn.write(cmd)
        self.tn.read_until('OK')
        #print('OK')

    def digitalWrite(self, *args):
        print "digitalWrite - should not be called"
